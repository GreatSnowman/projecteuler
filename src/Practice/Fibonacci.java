/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Practice;

/**
 *
 * @author l1021357
 */
public class Fibonacci {
    //Recursive method of calculating Fibonacci number
    public static void main(String [] args){
        //sets how long the applicaiton will loops
        int count = 34;
        //creates a 1D array
        int [] fib = new int[count];
        //Assigns initial values to the array
        fib[0] = 0;
        fib[1] = 1;
        
        for(int i = 2; i < count; i++){
            fib[i] = fib[i - 1] + fib[i - 2];
            System.out.println(fib[i] + "  ");
        }
//        for(int i = 2; i < count; i++){
//            
//        }
                
        
    }
    
}
