/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Practice;

/**
 *
 * @author l1021357
 */
//Iteration Method Fibonacci Sequence
public class Fibonacci2 {
    public static void main(String [] args){
        //sets how long the applicaiton will loops
        int count = 30;
        
        //Assigns initial values to the array
        Integer fib1 = 0;
        Integer fib2 = 1;
        Integer fib = 0;
        for(int i = 0; i < count; i++){
            fib = fib1;
            fib1 = fib2;
            fib2 = fib + fib2;
            System.out.println(fib1 + "  ");
        }
        
    }
}
