/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EULER1.Problem1to10;
//If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.

import java.util.ArrayList;


//Find the sum of all the multiples of 3 or 5 below 1000.
/**
 *
 * @author l1021357
 */
public class Problem1 {
   
    ArrayList<Integer> x = new ArrayList<Integer>();
    ArrayList<Integer> y = new ArrayList<Integer>();
    
    int sum1 = 0;
    int sum2 = 0;
    public Problem1() {
        
        
        for(int i = 0; i< 1000; i++ ){
            if(i % 3 == 0){
                x.add(i);
                sum1 +=i;
            }else if(i % 5 == 0){
                sum2 += i;
            }
        }
        int total = sum1 + sum2;
        System.out.println(total);
    }
    
    public static void main(String[] args){
        
        new Problem1();
        
    }
    
    
}
