/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EULER1.Problem1to10;

/**
 *
 * @author Kevin
 */
public class Problem10 {

    public int number;
    public int num=2;
    public long total;
    
    public Problem10() {
        while(num < 2000000){
          if(isPrime(num)){
              //System.out.println(num);
              total += num;
          }
          num ++;
      }
        System.out.println(total);
    }
    
    public boolean isPrime(int numbers){
        double root =  Math.sqrt(numbers);
        
        for(int i = 2;i <= root; i++){
            if(numbers % i ==0){
                return false;
            }
        }
        return true;
    }
    
    public static void main(String[] args){
        new Problem10();
    }
    
}
