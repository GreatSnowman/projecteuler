/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EULER1.Problem1to10;

/**
 *
 * @author l1021357
 */
public class Problem6 {

    int diff = 0;
    
    int sum = 0;
    
    int sqSum = 0;
    int sumSq = 0;
    
    public Problem6() {
        
        for(int i = 0; i <= 100; i++){
            
            int sq = i * i;
            sumSq = sumSq + sq;
            
            
            sum = sum + i;
            sqSum = sum * sum;
            
        }
        
        diff = sqSum - sumSq;
        
        System.out.println(diff);
        
    }
    
    public static void main(String [] args){
        new Problem6();
    }
    
}
