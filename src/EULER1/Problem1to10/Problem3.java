/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EULER1.Problem1to10;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author l1021357
 */
public class Problem3 {
    
    long fib = 600851475143L;
    
     public static List<Long> primeFactors(Long numbers) {
    long n = numbers;
    //creates an array list to store the different factors.
    List<Long> factors = new ArrayList<Long>();
    for (long i = 2; i <= n / i; i++) {
        //finds the factors by finding the mode of the number using 'i'
      while (n % i == 0) {
        factors.add(i);
        n /= i;
      }
    }
    if (n > 1) {
      factors.add(n);
    }
    return factors;
  }

  public static void main(String[] args) {
    System.out.println("Primefactors of 44");
    for (Long integer : primeFactors(600851475143L)) {
      System.out.println(integer);
    }
    
  }
} 