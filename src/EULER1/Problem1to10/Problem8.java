/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EULER1.Problem1to10;

import java.math.BigInteger;

/**
 *
 * @author Kevin
 */
public class Problem8 {

    public Integer[][] grid;
    String sNum = "73167176531330624919225119674426574742355349194934" +
"96983520312774506326239578318016984801869478851843" +
"85861560789112949495459501737958331952853208805511" +
"12540698747158523863050715693290963295227443043557" +
"66896648950445244523161731856403098711121722383113" +
"62229893423380308135336276614282806444486645238749" +
"30358907296290491560440772390713810515859307960866" +
"70172427121883998797908792274921901699720888093776" +
"65727333001053367881220235421809751254540594752243" +
"52584907711670556013604839586446706324415722155397" +
"53697817977846174064955149290862569321978468622482" +
"83972241375657056057490261407972968652414535100474" +
"82166370484403199890008895243450658541227588666881" +
"16427171479924442928230863465674813919123162824586" +
"17866458359124566529476545682848912883142607690042" +
"24219022671055626321111109370544217506941658960408" +
"07198403850962455444362981230987879927244284909188" +
"84580156166097919133875499200524063689912560717606" +
"05886116467109405077541002256983155200055935729725" +
"71636269561882670428252483600823257530420752963450";
   
    public Long[] iNum;
       
       Long total = (long) 1;
        Long answer = (long) 0;      
    public Problem8() {
       
        iNum = new Long[1000];
       for(int i = 0; i < 1000; i++){
           String[] stNum = sNum.split("");
         iNum[i] = Long.parseLong(stNum[i]);  
       }
       System.out.println(calc());
       
    }
    public Long calc(){
        for(int a = 0; a < iNum.length-13; a++){
           
            total = iNum[a] * iNum[a+1] * iNum[a+2] * iNum[a+3] * iNum[a+4]
                    * iNum[a+5] * iNum[a+6] * iNum[a+7] * iNum[a+8] * iNum[a+9]
                     * iNum[a+10] * iNum[a+11] * iNum[a+12];
            
           
            if(total > answer && total != 0){
                answer = total;
                System.out.println(answer);
                
            }
       }
        return answer;
    }
    public static void main(String[] args){
        new Problem8();
    }
}
