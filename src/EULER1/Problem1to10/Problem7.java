/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EULER1.Problem1to10;

/**
 *
 * @author Kevin
 */
public final class Problem7 {

    public int number;
    public int num=2;
    
    public Problem7() {
      while(number < 10001){
          if(isPrime(num)){
              System.out.println(num);
              number++;
          }
          num++;
      }
    }
    
    public boolean isPrime(int numbers){
        double root = Math.sqrt(numbers);
        
        for(int i = 2;i <= root; i++){
            if(numbers % i ==0){
                return false;
            }
        }
        return true;
    }
    
    
     public static void main(String[] args) {
        new Problem7();
    
  }
}
