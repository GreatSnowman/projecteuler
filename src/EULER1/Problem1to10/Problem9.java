/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EULER1.Problem1to10;

/**
 *
 * @author Kevin
 */
public class Problem9 {
    
    double a,b, c, cSqd;
    int total = 1000, ans;
    
    public Problem9(){
        
        for( a = 1; a < 500; a++){
            for(b = 1; b<500; b++){
                
                cSqd = (a*a) + (b*b);
                
                c = Math.sqrt(cSqd);
               
                if(a + b + c == total){
                    ans = (int) (a * b * c);
                    System.out.println(ans);
                 
                }
                
            }
        }
    }
    
    
    public static void main(String[] args){
        new Problem9();
    }
    
}
